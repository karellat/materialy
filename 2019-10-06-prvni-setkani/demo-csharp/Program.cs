﻿using System;
using System.Text;

namespace demo_csharp
{
	class Program
	{
		static void Main(string[] args)
		{
			Console.WriteLine("Čau, jak tě mám oslovovat?");
			var name = Console.ReadLine();

			Console.WriteLine("Jseš muž, nebo žena?");
			var gender = Console.ReadLine();

			var text = "";

			if (gender == "žena") {
				text = $"{name}, jseš kočka!";
			} else if (gender == "muž") {
				text = $"{name}, jseš borec!";
			} else {
				text = $"{name}, vždycky jsem chtěl být {gender}!";
			}

			Console.WriteLine("╔" + Repeat("═", text.Length + 2) + "╗");
			Console.WriteLine("║ " + text + " ║");
			Console.WriteLine("╚" + Repeat("═", text.Length + 2) + "╝");
		}

		static string Repeat(string text, int number) {
			StringBuilder ss = new StringBuilder();

			for (int i = 0; i < number; ++i) {
				ss.Append(text);
			}

			return ss.ToString();
		}
	}
}
