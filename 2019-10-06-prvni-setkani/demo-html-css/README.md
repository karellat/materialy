# Demo HTML/CSS

Jediné co potřebuješ je webový prohlížeč a v něm otevřít soubor `index.html`

## Hraj si :-)

V prohlížeči stiskni `F12`, nebo klikni na nějaký prvek stránky pravým talčítkem a dej Prozkoumat. To ti zobrazí vývojářské nástroje, které ti ukazují strukturu (HTML) stránky a když vybereš nějaký prvek (tag) tak vidíš i jeho styly. Všechno můžeš upravovat přímo v prohlížeči. Pokud chceš, aby změny zůstaly, musíš je upravit přímo v souborech `index.html` nebo `styles.css`.
